# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import base64

from django.http import HttpResponse
from django.shortcuts import render, redirect
# Create your views here.
from django.template.loader import get_template
from xhtml2pdf import pisa

from offerletter.forms import AddForm, ImgForm
from offerletter.models import Profile, Image
from offerletter.utils import render_to_pdf
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.template import RequestContext
from django.conf import settings
# import ho.pisa as pisa
import cStringIO as StringIO
import cgi
import os


def home_age(requets):
    print('home_age called')
    return render(requets, 'offerletter/home.html')


#
#
# def profile_page(request):
#     print('profile_page called')
#     return render(request, 'offerletter/profile-page.html')
#
#
# def adding_page(request):
#     print('adding_page called')
#     if request.method == 'POST':
#         form = AddForm(request.POST)
#         if form.is_valid():
#             post = form.save(commit=False)
#             post.save()
#             return redirect('details_page')
#     else:
#         form = AddForm
#     return render(request, 'offerletter/add-page.html', {'form': form})
#
#
def details_page(request):
    print('details_page called')
    details = Profile.objects.all()
    return render(request, 'offerletter/profile-details.html', {'posts': details})


def dashboard_page(request):
    print('dashboard_page called')
    return render(request, 'offerletter/dashboard.html')


def profile_page(request):
    print('profile_page called')
    return render(request, 'offerletter/profile-page.html')


def adding_cand(request):
    print('adding_page called')
    if request.method == 'POST':
        form = AddForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            details = Profile.objects.all()
            return render(request, 'offerletter/profile-details.html', {'posts': details})
    else:
        print('adding_cand called')
        form = AddForm
    return render(request, 'offerletter/add-page.html', {'form': form})


def offer_letter(request):
    print('offer letter called')
    return render(request, 'offerletter/offer.html')


def gen_offer(request):
    print('gen_offer called')
    details = Profile.objects.all()
    return render(request, 'offerletter/get_list_offer.html', {'posts': details})


def take_candidate(request, id):
    print('takecandidate called', request.POST)
    data = Profile.objects.get(id=id)
    try:
        image = Image.objects.all()
        encoded_string = base64.b64encode(image)
        print('details', encoded_string)
    except Exception as e:
        print('exception as e', e)
    template = get_template('offerletter/offer_letter_temp.html')
    print('after the template')
    context = {
        "first_name": data.first_name,
        "mobile_number": data.mobile_number,
        "email": data.email,
        "father_name": data.father_name,
        "city": data.city,
        "district": data.district,
        "state": data.state,
        "pincode": data.pin_code,
        "image": image[0].image
    }
    html = template.render(context)
    pdf = render_to_pdf('offerletter/offer_letter_temp.html', context)
    # return render(request, 'offerletter/offer_letter_temp.html')
    if pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        filename = 'offer_letter_%s.pdf' % (data.first_name)
        content = "inline; filename='%s'" % (filename)
        response['Content-Disposition'] = content
        return response
    return HttpResponse('Not Found')
    print('after the pdf')
    return pdf
    # return HttpResponse(pdf, content_type='application/pdf')
    # return render(request, 'offerletter/offer_letter_temp.html',{'data': data})


def save_image(request):
    print('add image called')
    if request.method == 'POST':
        print('after the if request post method')
        form = ImgForm(request.POST, request.FILES)
        print('after the img form FILES')
        if form.is_valid():
            print('after the form is valid')
            post = form.save(commit=False)
            print('afetre comit false')
            post.save()
            print('after the save')
            details = Image.objects.all()
            # print('images', details_page())
            return render(request, 'offerletter/offer.html', {'data': details})
    else:
        form = ImgForm
    return render(request, 'offerletter/addimage.html', {'form': form})
