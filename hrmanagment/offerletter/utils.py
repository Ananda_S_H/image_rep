import cgi
from io import BytesIO

import os
from StringIO import StringIO
from django.conf import settings
from django.http import HttpResponse
from django.template import RequestContext
from django.template.loader import get_template, render_to_string

from xhtml2pdf import pisa


def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None


def dm_monthly(request, year, month):
    html = render_to_string('offerletter/offer_letter_temp.html', {'pagesize': 'A4', },
                            context_instance=RequestContext(request))
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources)
    if not pdf.err:
        return HttpResponse(result.getvalue(), mimetype='application/pdf')
    return HttpResponse('Gremlins ate your pdf! %s' % cgi.escape(html))


def fetch_resources(uri, rel):
    path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))

    return path

# def take_candidate(request, id):
#     print('takecandidate called',request.POST)
#     data = Profile.objects.get(id=id)
#     try:
#         image = Image.objects.all().values()[0]
#         encoded_string = base64.b64encode(image)
#         print('details', encoded_string)
#     except Exception as e:
#         print('exception as e', e)
#     template = get_template('offerletter/offer_letter_temp.html')
#     print('after the template')
#     context = {
#         "first_name": data.first_name,
#         "mobile_number": data.mobile_number,
#         "email": data.email,
#         "symbol":image
#     }
#     html = template.render(context)
#     pdf = render_to_pdf('offerletter/offer_letter_temp.html', context)
#     if pdf:
#         response = HttpResponse(pdf, content_type='application/pdf')
#         filename = 'offer_letter_%s.pdf' %(data.first_name)
#         content = "inline; filename='%s'" %(filename)
#         response['Content-Disposition'] = content
#         return response
#     return HttpResponse('Not Found')
