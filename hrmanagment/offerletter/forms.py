from django import forms

from .models import Profile, Image


class AddForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('first_name', 'mobile_number', 'email', 'father_name', 'city', 'district', 'state', 'pin_code')


from registration.forms import RegistrationForm


class RegForm(RegistrationForm):
    def __init__(self, *args, **kwargs):
        super(RegForm, self).__init__(*args, **kwargs)

        for fieldname in ['usename', 'email', 'password1', 'password2', ]:
            self.fields[fieldname].help_text = None
            self.fields[fieldname].lables = False


class ImgForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ('name', 'image')

# class UserCreateForm(UserCreationForm):
#     email = forms.EmailField(required=True)
#
#     def __init__(self, *args, **kwargs):
#         super(UserCreateForm, self).__init__(*args, **kwargs)
#             self.fields['email'].help_text = ''
