    # -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Profile(models.Model):
    first_name = models.CharField(max_length=50)
    mobile_number = models.BigIntegerField()
    email = models.EmailField()
    father_name = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    district = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    pin_code = models.BigIntegerField()


class Image(models.Model):
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='offerletter/')
