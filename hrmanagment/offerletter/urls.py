from django.conf.urls import url

app_name = 'offerletter'

from offerletter import views

urlpatterns = [
    url(r'^$', views.home_age, name='home_age'),
    url(r'^dashboard', views.dashboard_page, name='dashboard_page'),
    url(r'^profile', views.profile_page, name='profile'),
    # url(r'^profile$', views.profile_page, name='profile_page'),
    url(r'^add', views.adding_cand, name='add_cand'),
    url(r'^details', views.details_page, name='cand_list'),
    url(r'^offer', views.offer_letter, name='offerltr'),
    url(r'^genoffer', views.gen_offer, name='gen_offer'),
    url(r'^takecand/(?P<id>[0-9]+)/', views.take_candidate, name='takecandidate'),
    url(r'^image', views.save_image, name='save_image')

]
